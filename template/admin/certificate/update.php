<?php
// Include database connection file
require_once "../../../connection.php";
if (count($_POST) > 0) {
    mysqli_query($conn, "UPDATE databank set semester='" . $_POST['semester'] . "', module_name='" . $_POST['module_name'] . "', module_code='" . $_POST['module_code'] . "'  WHERE databank_id='" . $_POST['databank_id'] . "'");
    header("location: btca_s2.php");
    exit();
}
$result = mysqli_query($conn, "SELECT * FROM databank WHERE databank_id='" . $_GET['id'] . "'");
$row = mysqli_fetch_array($result);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Edit Modules</title>
    <?php include "head.php"; ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Edit Modules</h2>
                </div>
                <p>Please edit the input values and submit to update the record.</p>
                <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                    <div class="form-group ">
                        <label>Semester</label>
                        <input type="number" name="semester" class="form-control" value="<?php echo $row["semester"]; ?>" maxlength="30" required="">
                    </div>
                    <div class="form-group ">
                        <label>Course Name</label>
                        <input type="text" name="course_name" class="form-control" value="<?php echo $row["course_name"]; ?>" maxlength="30" required="">
                    </div>
                    <div class="form-group">
                        <label>Module Name</label>
                        <input type="text" name="module_name" class="form-control" value="<?php echo $row["module_name"]; ?>" maxlength="50" required="">
                    </div>
                    <div class="form-group ">
                        <label>Module Code</label>
                        <input type="type" name="module_code" class="form-control" value="<?php echo $row["module_code"]; ?>" maxlength="30" required="">
                    </div>
                    <div class="form-group ">
                        <label> Question</label>
                        <input type="type" name="question" class="form-control" value="<?php echo $row["question"]; ?>" maxlength="30" required="">
                    </div>


                    <input type="hidden" name="databank_id" value="<?php echo $row["databank_id"]; ?>" />
                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="../certificate.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</body>

</html>