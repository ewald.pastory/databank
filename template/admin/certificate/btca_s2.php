<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <?php include "head.php"; ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <div class="page-header clearfix">
                    <h2 class="pull-left"> BTCA SEMESTER II</h2>
                    <a href="add.php" class="btn btn-success pull-right">Add New Module</a>
                </div>
                <?php
                include_once '../../../connection.php';
                $result = mysqli_query($conn, "SELECT databank.semester, databank.module_name, databank.module_code, databank.course_name, databank.databank_id,databank.question FROM databank  WHERE databank.semester ='2' and databank.course_name='btca'");
                ?>
                <?php
                if (mysqli_num_rows($result) > 0) {
                ?>
                    <table class='table table-bordered table-striped'>
                        <tr>
                            <td>Module Name</td>
                            <td>Module Code</td>
                            <td>Question</td>


                        </tr>
                        <?php
                        $i = 0;
                        while ($row = mysqli_fetch_array($result)) {
                        ?>
                            <tr>
                                <td><?php echo $row["module_name"]; ?></td>
                                <td><?php echo $row["module_code"]; ?></td>
                                <td><?php echo $row["question"]; ?></td>



                                <td><a href="update.php?id=<?php echo $row["databank_id"]; ?> " title='Update Record' style="color:green;"><i class='fa fa-pencil'></i> edit module</a>
                                    <a href="delete.php?id=<?php echo $row["databank_id"]; ?>" title='Delete Record' style="color:red;"><i class='material-icons'><span class='glyphicon glyphicon-trash'></span>delete module</a>
                                    <a href="view_qns.php?id=<?php echo $row["module_code"]; ?>" title='View Questions' style="color:orangered;"><i class='material-icons'><span class='glyphicon glyphicon-eye-open'></span>view question</a>

                                </td>
                            </tr>
                        <?php
                            $i++;
                        }
                        ?>
                    </table>
                <?php
                } else {
                    echo "No result found";
                }
                ?>
            </div>
        </div>
    </div>
</body>

</html>