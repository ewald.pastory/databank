<?php
require_once "../../connection.php";
if (isset($_POST['save'])) {
    $question = $_POST['question'];
   

    $sql = "INSERT INTO module (module_name, module_code )
VALUES ('$module_name','$module_code')";
    if (mysqli_query($conn, $sql)) {
        header("location: index.php");
        exit();
    } else {
        echo "Error: " . $sql . "
" . mysqli_error($conn);
    }
    mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <?php include "head.php"; ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Create Record</h2>
                </div>
                <p>Please fill this form and submit to add employee record to the database.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group">
                        <label>Module Name</label>
                        <input type="text" name="module_code" class="form-control" value="" maxlength="50" required="">
                    </div>
                    <div class="form-group ">
                        <label>Module Code</label>
                        <input type="text" name="module_code" class="form-control" value="" maxlength="30" required="">
                    </div>

                    <input type="submit" class="btn btn-primary" name="save" value="submit">
                    <a href="index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</body>

</html>