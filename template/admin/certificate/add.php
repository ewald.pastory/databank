<?php
require_once "../../../connection.php";
if (isset($_POST['save'])) {
    $module_name = $_POST['module_name'];
    $module_code = $_POST['module_code'];
    $course_name = $_POST['course_name'];
    $semester = $_POST['semester'];
    $question = $_POST['question'];


    $sql1 = "INSERT INTO databank (module_name, module_code,course_name,semester,question ) 
VALUES ('$module_name','$module_code','$course_name','$semester','$question') ";



    if (mysqli_query($conn, $sql1) ) {
        header("location: ../certificate.php");
        exit();
    } else {
        echo "Error: " . $sql1 . "
" . mysqli_error($conn);
    }
    mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <?php include "head.php"; ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Create Record</h2>
                </div>
                <p>Please fill this form and submit to add programme record to the database.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group ">
                        <label>Semester</label>
                        <input type="number" name="semester" class="form-control" value="" maxlength="1" required="" placeholder="Eg: 1 or 2 or 3 or 4">
                    </div>
                    <div class="form-group ">
                        <label> Course Name</label>
                        <input type="text" name="course_name" class="form-control" value="" maxlength="30" required="" placeholder="Eg: btca or bbm or odcs">
                    </div>
                    <div class="form-group">
                        <label>Module Name</label>
                        <input type="text" name="module_name" class="form-control" value="" maxlength="50" required="">
                    </div>
                    <div class="form-group ">
                        <label>Module Code</label>
                        <input type="text" name="module_code" class="form-control" value="" maxlength="30" required="">
                    </div>


                    <div class="form-group ">
                        <label>Question</label>
                        <input type="text" name="question" class="form-control" value="" required="">
                    </div>

                    <input type="submit" class="btn btn-primary" name="save" value="submit">
                    <a href="certificate.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</body>

</html>