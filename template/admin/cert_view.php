<?php
session_start();
include "../../db_conn.php";


if (isset($_SESSION['username']) && isset($_SESSION['id'])) {   ?>




    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>DATA BANK</title>

        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <link href="css/accordian.css" rel="stylesheet">
        <style>
            .modal-full {
                width: 90%;
                height: 100%;
                margin: 0;
                padding: 16px 16px;

            }

            .modal-full .modal-content {
                height: auto;
                min-height: 100%;
                border-radius: 0;

            }

            .table-wrapper {
                background: #fff;
                padding: 20px 25px;
                margin: 30px 0;
                border-radius: 1px;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.247);

            }

            .table-title {
                padding-bottom: 15px;
                background: linear-gradient(40deg, #2096ff, #05ffa3) !important;
                color: #fff;
                padding: 16px 30px;
                margin: -20px -25px 10px;
                border-radius: 1px 1px 0 0;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.247);
            }

            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }

            .table-title .btn-group {
                float: right;
            }

            .table-title .btn {
                color: #fff;
                float: right;
                font-size: 13px;
                border: none;
                min-width: 50px;
                border-radius: 1px;
                border: none;
                outline: none !important;
                margin-left: 10px;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.247);
            }

            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }

            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }

            table.table tr th,
            table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }

            table.table tr th:first-child {
                width: 60px;
            }

            table.table tr th:last-child {
                width: 100px;
            }

            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }

            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }

            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }

            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }

            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
                outline: none !important;
            }

            table.table td a:hover {
                color: #2196F3;
            }

            table.table td a.edit {
                color: #FFC107;
            }

            table.table td a.delete {
                color: #F44336;
            }

            table.table td i {
                font-size: 19px;
            }

            table.table .avatar {
                border-radius: 1px;
                vertical-align: middle;
                margin-right: 10px;
            }

            .pagination {
                float: right;
                margin: 0 0 5px;
            }

            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 1px !important;
                text-align: center;
                padding: 0 6px;
            }

            .pagination li a:hover {
                color: #666;
            }

            .pagination li.active a,
            .pagination li.active a.page-link {
                background: #03A9F4;
            }

            .pagination li.active a:hover {
                background: #0397d6;
            }

            .pagination li.disabled i {
                color: #ccc;
            }

            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }

            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

            /* Custom checkbox */
            .custom-checkbox {
                position: relative;
            }

            .custom-checkbox input[type="checkbox"] {
                opacity: 0;
                position: absolute;
                margin: 5px 0 0 3px;
                z-index: 9;
            }

            .custom-checkbox label:before {
                width: 18px;
                height: 18px;
            }

            .custom-checkbox label:before {
                content: '';
                margin-right: 10px;
                display: inline-block;
                vertical-align: text-top;
                background: white;
                border: 1px solid #bbb;
                border-radius: 1px;
                box-sizing: border-box;
                z-index: 2;
            }

            .custom-checkbox input[type="checkbox"]:checked+label:after {
                content: '';
                position: absolute;
                left: 6px;
                top: 3px;
                width: 6px;
                height: 11px;
                border: solid #000;
                border-width: 0 3px 3px 0;
                transform: inherit;
                z-index: 3;
                transform: rotateZ(45deg);
            }

            .custom-checkbox input[type="checkbox"]:checked+label:before {
                border-color: #03A9F4;
                background: #03A9F4;
            }

            .custom-checkbox input[type="checkbox"]:checked+label:after {
                border-color: #fff;
            }

            .custom-checkbox input[type="checkbox"]:disabled+label:before {
                color: #b8b8b8;
                cursor: auto;
                box-shadow: none;
                background: #ddd;
            }

            /* Modal styles */
            .modal .modal-dialog {
                max-width: 100%;
            }

            .modal .modal-header,
            .modal .modal-body,
            .modal .modal-footer {
                padding: 20px 30px;
            }

            .modal .modal-content {
                border-radius: 1px;
            }

            .modal .modal-footer {
                background: #ecf0f1;
                border-radius: 0 0 1px 1px;
            }

            .modal .modal-title {
                display: inline-block;
            }

            .modal .form-control {
                border-radius: 1px;
                box-shadow: none;
                border-color: #dddddd;
            }

            .modal textarea.form-control {
                resize: vertical;
            }

            .modal .btn {
                border-radius: 1px;
                min-width: 100px;
            }

            .modal form label {
                font-weight: normal;

            }

            .logo_rotate {
                -webkit-animation: rotation 2s infinite linear;
            }

            @-webkit-keyframes rotation {
                from {
                    -webkit-transform: rotate(0deg);
                }

                to {
                    -webkit-transform: rotate(359deg);
                }
            }
        </style>
        <script>
            $(document).ready(function() {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function() {
                    if (this.checked) {
                        checkbox.each(function() {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function() {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function() {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });

            });
        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>

    <body id="page-top">







        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                    <div class="sidebar-brand-icon rotate-n-0">
                        <img src="../../img/small.png" alt="logo" style="width:50px;">

                    </div>
                    <div class="sidebar-brand-text mx-3">DATA BANK</div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <li class="nav-item active">
                    <a class="nav-link" href="index.html">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Interface
                </div>

                <!-- USER SETTINGS -->

                <li class="nav-item">
                    <?php if ($_SESSION['role'] == 'admin') : ?>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i class="fas fa-fw fa-cog"></i>
                            <span>Users Setting</span>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <h6 class="collapse-header">User Settings</h6>
                                <a class="collapse-item" href="../../admin/users/index.php">Users</a>

                            </div>
                        </div>
                    <?php endif;  ?>
                </li>


                <!-- USER SETTINGS -->
                <!-- USER SETTINGS -->



                <!-- USER SETTINGS -->
                <!-- Modules Settings -->
                <li class="nav-item">
                    <?php if ($_SESSION['role'] == ('lecturer') || ('examiner')) : ?>

                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i class="fa fa-user"></i>
                            <span>Module Settings</span>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <h6 class="collapse-header">Modules</h6>
                                <a class="collapse-item" href="../../admin/module/index.php">View Modules</a>

                            </div>
                        </div>
                    <?php endif;  ?>

                </li>


                <!-- Module Settings -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                        <i class="fas fa-fw fa-book"></i>
                        <span>Course Lists</span>
                    </a>
                    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">

                            <a class="collapse-item" href="certificate.php">Certificate</a>
                            <a class="collapse-item" href="diploma.php">Diploma</a>
                            <a class="collapse-item" href="bacheror.php">Bacheror's Degree</a>

                            <a class="collapse-item" href="msc.php">Masters </a>

                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Addons
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Pages</span>
                    </a>
                    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Login Screens:</h6>
                            <a class="collapse-item" href="login.html">Login</a>
                            <a class="collapse-item" href="register.html">Register</a>
                            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                            <div class="collapse-divider"></div>
                            <h6 class="collapse-header">Other Pages:</h6>
                            <a class="collapse-item" href="404.html">404 Page</a>
                            <a class="collapse-item" href="blank.html">Blank Page</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Charts -->
                <li class="nav-item">
                    <a class="nav-link" href="charts.html">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Charts</span></a>
                </li>

                <!-- Nav Item - Tables -->
                <li class="nav-item">
                    <a class="nav-link" href="tables.html">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Tables</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

                <!-- Sidebar Message -->
                <!-- <div class="sidebar-card d-none d-lg-flex">
                    <img class="sidebar-card-illustration mb-2" src="img/undraw_rocket.svg" alt="...">
                    <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>
                    <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>
                </div> -->

            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->
                        <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            <?php if ($_SESSION['role'] == 'admin') : ?>
                                <div class="input-group">
                                    <img src="../../img/small.png" alt="logo" style="width:50px;" class="logo_rotate"> &nbsp; &nbsp;

                                    <h2 style="color:chocolate;">Administrator</h2>
                                </div>
                            <?php endif;  ?>
                            <?php if ($_SESSION['role'] == 'lecturer') : ?>
                                <div class="input-group">

                                    <h2 style="color:chocolate;">Lecturer</h2>
                                </div>
                            <?php endif;  ?>
                            <?php if ($_SESSION['role'] == 'examiner') : ?>
                                <div class="input-group">

                                    <h2 style="color:chocolate;">Examination Officer</h2>
                                </div>
                            <?php endif;  ?>
                        </form>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                            <li class="nav-item dropdown no-arrow d-sm-none">
                                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-search fa-fw"></i>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto w-100 navbar-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fas fa-search fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>





                            <!-- Nav Item - Alerts -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bell fa-fw"></i>
                                    <!-- Counter - Alerts -->
                                    <span class="badge badge-danger badge-counter">3+</span>
                                </a>
                                <!-- Dropdown - Alerts -->
                                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                                    <h6 class="dropdown-header">
                                        Alerts Center
                                    </h6>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-primary">
                                                <i class="fas fa-file-alt text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">December 12, 2019</div>
                                            <span class="font-weight-bold">A new monthly report is ready to download!</span>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-success">
                                                <i class="fas fa-donate text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">December 7, 2019</div>
                                            $290.29 has been deposited into your account!
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-warning">
                                                <i class="fas fa-exclamation-triangle text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">December 2, 2019</div>
                                            Spending Alert: We've noticed unusually high spending for your account.
                                        </div>
                                    </a>
                                    <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                                </div>
                            </li>

                            <!-- Nav Item - Messages -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-envelope fa-fw"></i>
                                    <!-- Counter - Messages -->
                                    <span class="badge badge-danger badge-counter">7</span>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                                    <h6 class="dropdown-header">
                                        Message Center
                                    </h6>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="img/undraw_profile_1.svg" alt="...">
                                            <div class="status-indicator bg-success"></div>
                                        </div>
                                        <div class="font-weight-bold">
                                            <div class="text-truncate">Hi there! I am wondering if you can help me with a
                                                problem I've been having.</div>
                                            <div class="small text-gray-500">Emily Fowler · 58m</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="img/undraw_profile_2.svg" alt="...">
                                            <div class="status-indicator"></div>
                                        </div>
                                        <div>
                                            <div class="text-truncate">I have the photos that you ordered last month, how
                                                would you like them sent to you?</div>
                                            <div class="small text-gray-500">Jae Chun · 1d</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="img/undraw_profile_3.svg" alt="...">
                                            <div class="status-indicator bg-warning"></div>
                                        </div>
                                        <div>
                                            <div class="text-truncate">Last month's report looks great, I am very happy with
                                                the progress so far, keep up the good work!</div>
                                            <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="...">
                                            <div class="status-indicator bg-success"></div>
                                        </div>
                                        <div>
                                            <div class="text-truncate">Am I a good boy? The reason I ask is because someone
                                                told me that people say this to all dogs, even if they aren't good...</div>
                                            <div class="small text-gray-500">Chicken the Dog · 2w</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
                                </div>
                            </li>

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><b> <?= $_SESSION['name'] ?></b></span>
                                    <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Profile
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Settings
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Activity Log
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Logout
                                    </a>
                                </div>
                            </li>

                        </ul>

                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->

                    <!-- Bacheror heading -->

                    <!-- MODAL STARTS -->
                    <div class="modal fade" id="studentaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Module </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <form action="cert_insert.php" method="POST">

                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label> Semester </label>
                                            <input type="number" name="semester" class="form-control" placeholder="Eg: 1 or 2 or 3 or 4" required="">
                                        </div>

                                        <div class="form-group">
                                            <label> Course Name</label>
                                            <input type="text" name="course_name" class="form-control" placeholder="Eg: BTCA or ODCS OR BBM" required="">
                                        </div>

                                        <div class="form-group">
                                            <label> Module Name </label>
                                            <input type="text" name="module_name" class="form-control" placeholder="Enter Module Name" required="">
                                        </div>

                                        <div class="form-group">
                                            <label> Module Code </label>
                                            <input type="text" name="module_code" class="form-control" placeholder="Enter Module Code" required="">
                                        </div>
                                        <div class="form-group">
                                            <label> Question </label>
                                            <textarea name="question" class="form-control" placeholder="Enter Phone Number" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" name="insertdata" class="btn btn-primary">Save Data</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <!-- EDIT POP UP FORM (Bootstrap MODAL) -->
                    <div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Edit Module </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <form action="cert_update.php" method="POST">

                                    <div class="modal-body">

                                        <input type="hidden" name="databank_id" id="databank_id">

                                        <div class="form-group">
                                            <label>Semester </label>
                                            <input type="number" name="semester" id="semester" class="form-control" placeholder="Enter Semester">
                                        </div>

                                        <div class="form-group">
                                            <label> Course Name </label>
                                            <input type="text" name="course_name" id="course_name" class="form-control" placeholder="Enter Course Name">
                                        </div>

                                        <div class="form-group">
                                            <label> Module Name </label>
                                            <input type="text" name="module_name" id="module_name" class="form-control" placeholder="Enter Module Name">
                                        </div>

                                        <div class="form-group">
                                            <label> Module Code </label>
                                            <input type="text" name="module_code" id="module_code" class="form-control" placeholder="Enter Module Code ">
                                        </div>
                                        <div class="form-group">
                                            <label> Question </label>
                                            <textarea name="question" class="form-control" id="question" placeholder="Enter Question" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" name="updatedata" class="btn btn-primary">Update Data</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <!-- DELETE POP UP FORM (Bootstrap MODAL) -->
                    <div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Delete Module Data </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <form action="deletecode.php" method="POST">

                                    <div class="modal-body">

                                        <input type="hidden" name="delete_id" id="delete_id">

                                        <h4> Do you want to Delete this Data ??</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> NO </button>
                                        <button type="submit" name="deletedata" class="btn btn-primary"> Yes !! Delete it. </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>


                    <!-- VIEW POP UP FORM (Bootstrap MODAL) -->
                    <div class="modal fade" id="viewmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> View Question Data </h5>

                                </div>

                                <form action="deletecode.php" method="POST">

                                    <div class="modal-body">

                                        <div class="card">
                                            <div class="card-body">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#studentaddmodal">
                                                    Add Module
                                                </button>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-body">

                                                <?php
                                                include "../../db_conn.php";

                                                $query = "SELECT databank.semester, databank.module_name, databank.module_code, databank.course_name, databank.databank_id,databank.question FROM databank  WHERE databank.semester ='2' and databank.course_name='btca' ";
                                                $query_run = mysqli_query($conn, $query);
                                                ?>
                                                <table id="datatableid" class="table table-bordered table-dark">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">ID</th>


                                                            <th scope="col"> Question </th>

                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    if ($query_run) {
                                                        foreach ($query_run as $row) {
                                                    ?>
                                                            <tbody>
                                                                <tr>
                                                                    <td> <?php echo $row['databank_id']; ?> </td>

                                                                    <td> <?php echo $row['question']; ?> </td>

                                                                    <!-- <td>
                                                                        <button type="button" class="btn btn-info viewbtn"> VIEW </button>
                                                                    </td> -->
                                                                    <td>
                                                                        <button type="button" class="btn btn-success editbtn"> EDIT </button>
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-danger deletebtn"> DELETE </button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                    <?php
                                                        }
                                                    } else {
                                                        echo "No Record Found";
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>

                                        <p id="course_name"> </p>
                                        <h4 id="module_name"> <?php echo ''; ?> </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> CLOSE </button>
                                        <!-- <button type="submit" name="deletedata" class="btn btn-primary"> Yes !! Delete it. </button> -->
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>


                    <div class="container" width="100%">
                        <div class="jumbotron" width="100%">
                            <div class="card">
                                <h2> BTCA SEMESTER II</h2>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#studentaddmodal">
                                        Add Module
                                    </button>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">

                                    <?php
                                    include "../../db_conn.php";

                                    $query = "SELECT databank.semester, databank.module_name, databank.module_code, databank.course_name, databank.databank_id,databank.question FROM databank  WHERE databank.semester ='2' and databank.course_name='btca' ";
                                    $query_run = mysqli_query($conn, $query);
                                    ?>
                                    <table id="datatableid" class="table table-bordered table-dark">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>

                                                <th scope="col"> Module Name </th>
                                                <th scope="col"> Module Code </th>
                                                <th scope="col"> Question </th>

                                            </tr>
                                        </thead>
                                        <?php
                                        if ($query_run) {
                                            foreach ($query_run as $row) {
                                        ?>
                                                <tbody>
                                                    <tr>
                                                        <td> <?php echo $row['databank_id']; ?> </td>
                                                        <td> <?php echo $row['module_name']; ?> </td>
                                                        <td> <?php echo $row['module_code']; ?> </td>
                                                        <td> <?php echo $row['question']; ?> </td>

                                                        <td>
                                                            <button type="button" data-row="" class="btn btn-info viewbtn"> VIEW </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-success editbtn"> EDIT </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger deletebtn"> DELETE </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                        <?php
                                            }
                                        } else {
                                            echo "No Record Found";
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>



                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

                    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
                    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

                    <script>
                        $(document).ready(function() {

                            $('.viewbtn').on('click', function() {
                                $('#viewmodal').modal('show');
                                $.ajax({ //create an ajax request to display.php
                                    type: "GET",
                                    url: "display.php",
                                    dataType: "html", //expect html to be returned                
                                    success: function(response) {
                                        $("#responsecontainer").html(response);
                                        //alert(response);
                                    }
                                });
                            });

                        });
                    </script>


                    <script>
                        $(document).ready(function() {

                            $('#datatableid').DataTable({
                                "pagingType": "full_numbers",
                                "lengthMenu": [
                                    [10, 25, 50, -1],
                                    [10, 25, 50, "All"]
                                ],
                                responsive: true,
                                language: {
                                    search: "_INPUT_",
                                    searchPlaceholder: "Search Your Data",
                                }
                            });

                        });
                    </script>

                    <script>
                        $(document).ready(function() {

                            $('.deletebtn').on('click', function() {

                                $('#deletemodal').modal('show');

                                $tr = $(this).closest('tr');

                                var data = $tr.children("td").map(function() {
                                    return $(this).text();
                                }).get();

                                console.log(data);

                                $('#delete_id').val(data[0]);

                            });
                        });
                    </script>

                    <script>
                        $(document).ready(function() {

                            $('.editbtn').on('click', function() {

                                $('#editmodal').modal('show');

                                $tr = $(this).closest('tr');

                                var data = $tr.children("td").map(function() {
                                    return $(this).text();
                                }).get();

                                console.log(data);

                                $('#databank_id').val(data[0]);
                                $('#semester').val(data[1]);
                                $('#course_name').val(data[2]);
                                $('#module_name').val(data[3]);
                                $('#module_code').val(data[4]);
                                $('#question').val(data[5]);
                            });
                        });
                    </script>
                    <!-- MODAL ENDS HERE -->
                    <!-- The Modal -->



                    <!-- Must put our javascript files here to fast the page loading -->

                    <!-- jQuery library -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    <!-- Popper JS -->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
                    <!-- Bootstrap JS -->
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
                    <!-- Page Script -->
                    <script src="assets/js/scripts.js"></script>
    </body>

    </html>
<?php } else {
    header("Location: index.php");
} ?>