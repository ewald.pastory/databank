<?php
session_start();
include "../../db_conn.php";


if (isset($_SESSION['username']) && isset($_SESSION['id'])) {   ?>




    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>DATA BANK</title>

        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <link href="css/accordian.css" rel="stylesheet">
        <style>
            .logo_rotate {
                -webkit-animation: rotation 2s infinite linear;
            }

            @-webkit-keyframes rotation {
                from {
                    -webkit-transform: rotate(0deg);
                }

                to {
                    -webkit-transform: rotate(359deg);
                }
            }
        </style>

    </head>

    <body id="page-top">







        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                    <div class="sidebar-brand-icon rotate-n-0">
                        <img src="../../img/small.png" alt="logo" style="width:50px;">

                    </div>
                    <div class="sidebar-brand-text mx-3">DATA BANK</div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <li class="nav-item active">
                    <a class="nav-link" href="index.html">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Interface
                </div>

                <!-- USER SETTINGS -->

                <li class="nav-item">
                    <?php if ($_SESSION['role'] == 'admin') : ?>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i class="fas fa-fw fa-cog"></i>
                            <span>Users Setting</span>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <h6 class="collapse-header">User Settings</h6>
                                <a class="collapse-item" href="../../admin/users/index.php">Users</a>

                            </div>
                        </div>
                    <?php endif;  ?>
                </li>


                <!-- USER SETTINGS -->
                <!-- USER SETTINGS -->



                <!-- USER SETTINGS -->
                <!-- Modules Settings -->
                <li class="nav-item">
                    <?php if ($_SESSION['role'] == ('lecturer') || ('examiner')) : ?>

                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i class="fa fa-user"></i>
                            <span>Module Settings</span>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <h6 class="collapse-header">Modules</h6>
                                <a class="collapse-item" href="../../admin/module/index.php">View Modules</a>

                            </div>
                        </div>
                    <?php endif;  ?>

                </li>


                <!-- Module Settings -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                        <i class="fas fa-fw fa-book"></i>
                        <span>Course Lists</span>
                    </a>
                    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">

                            <a class="collapse-item" href="certificate.php">Certificate</a>
                            <a class="collapse-item" href="diploma.php">Diploma</a>
                            <a class="collapse-item" href="bacheror.php">Bacheror's Degree</a>

                            <a class="collapse-item" href="msc.php">Masters </a>

                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Addons
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Pages</span>
                    </a>
                    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <h6 class="collapse-header">Login Screens:</h6>
                            <a class="collapse-item" href="login.html">Login</a>
                            <a class="collapse-item" href="register.html">Register</a>
                            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                            <div class="collapse-divider"></div>
                            <h6 class="collapse-header">Other Pages:</h6>
                            <a class="collapse-item" href="404.html">404 Page</a>
                            <a class="collapse-item" href="blank.html">Blank Page</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Charts -->
                <li class="nav-item">
                    <a class="nav-link" href="charts.html">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Charts</span></a>
                </li>

                <!-- Nav Item - Tables -->
                <li class="nav-item">
                    <a class="nav-link" href="tables.html">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Tables</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

                <!-- Sidebar Message -->
                <!-- <div class="sidebar-card d-none d-lg-flex">
                    <img class="sidebar-card-illustration mb-2" src="img/undraw_rocket.svg" alt="...">
                    <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>
                    <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>
                </div> -->

            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->
                        <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            <?php if ($_SESSION['role'] == 'admin') : ?>
                                <div class="input-group">
                                    <img src="../../img/small.png" alt="logo" style="width:50px;" class="logo_rotate"> &nbsp; &nbsp;

                                    <h2 style="color:chocolate;">Administrator</h2>
                                </div>
                            <?php endif;  ?>
                            <?php if ($_SESSION['role'] == 'lecturer') : ?>
                                <div class="input-group">

                                    <h2 style="color:chocolate;">Lecturer</h2>
                                </div>
                            <?php endif;  ?>
                            <?php if ($_SESSION['role'] == 'examiner') : ?>
                                <div class="input-group">

                                    <h2 style="color:chocolate;">Examination Officer</h2>
                                </div>
                            <?php endif;  ?>
                        </form>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                            <li class="nav-item dropdown no-arrow d-sm-none">
                                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-search fa-fw"></i>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto w-100 navbar-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fas fa-search fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>





                            <!-- Nav Item - Alerts -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bell fa-fw"></i>
                                    <!-- Counter - Alerts -->
                                    <span class="badge badge-danger badge-counter">3+</span>
                                </a>
                                <!-- Dropdown - Alerts -->
                                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                                    <h6 class="dropdown-header">
                                        Alerts Center
                                    </h6>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-primary">
                                                <i class="fas fa-file-alt text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">December 12, 2019</div>
                                            <span class="font-weight-bold">A new monthly report is ready to download!</span>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-success">
                                                <i class="fas fa-donate text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">December 7, 2019</div>
                                            $290.29 has been deposited into your account!
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-warning">
                                                <i class="fas fa-exclamation-triangle text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">December 2, 2019</div>
                                            Spending Alert: We've noticed unusually high spending for your account.
                                        </div>
                                    </a>
                                    <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                                </div>
                            </li>

                            <!-- Nav Item - Messages -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-envelope fa-fw"></i>
                                    <!-- Counter - Messages -->
                                    <span class="badge badge-danger badge-counter">7</span>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                                    <h6 class="dropdown-header">
                                        Message Center
                                    </h6>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="img/undraw_profile_1.svg" alt="...">
                                            <div class="status-indicator bg-success"></div>
                                        </div>
                                        <div class="font-weight-bold">
                                            <div class="text-truncate">Hi there! I am wondering if you can help me with a
                                                problem I've been having.</div>
                                            <div class="small text-gray-500">Emily Fowler · 58m</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="img/undraw_profile_2.svg" alt="...">
                                            <div class="status-indicator"></div>
                                        </div>
                                        <div>
                                            <div class="text-truncate">I have the photos that you ordered last month, how
                                                would you like them sent to you?</div>
                                            <div class="small text-gray-500">Jae Chun · 1d</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="img/undraw_profile_3.svg" alt="...">
                                            <div class="status-indicator bg-warning"></div>
                                        </div>
                                        <div>
                                            <div class="text-truncate">Last month's report looks great, I am very happy with
                                                the progress so far, keep up the good work!</div>
                                            <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="dropdown-list-image mr-3">
                                            <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="...">
                                            <div class="status-indicator bg-success"></div>
                                        </div>
                                        <div>
                                            <div class="text-truncate">Am I a good boy? The reason I ask is because someone
                                                told me that people say this to all dogs, even if they aren't good...</div>
                                            <div class="small text-gray-500">Chicken the Dog · 2w</div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
                                </div>
                            </li>

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><b> <?= $_SESSION['name'] ?></b></span>
                                    <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Profile
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Settings
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Activity Log
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Logout
                                    </a>
                                </div>
                            </li>

                        </ul>

                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->

                    <!-- Bacheror heading -->



                    <link rel="stylesheet" href="style.css">

                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    <script src="common.js"></script>





                    <div class="container">

                        <div class="container-inner">

                            <!-- Section Title -->

                            <div class="section-title">

                                <h1 STYLE="text-align:centre;">BASIC CERTIFICATE COURSES</h1>

                            </div>

                            <!-- Tab / Collapse Section -->

                            <div class="tab-container">

                                <!-- Tab 1 -->

                                <div class="tab-accordian">

                                    <div class="titleWrapper inactive">
                                        <h3>Basic Technician Certificate in Accountancy</h3>

                                        <div class="collapse-icon">
                                            <div class="acc-close"> </div>
                                            <div class="acc-open"></div>
                                        </div>
                                    </div>

                                    <div id="descwrapper" class="desWrapper">
                                        <div class="titleWrapper inactive">
                                            <h3>Semester 1</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Chemistry</a></h5>

                                        </div>

                                        <div class="titleWrapper inactive">
                                            <h3 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="cert_view.php" style="text-decoration:none;color:grey;">
                                                    Semester 2</h3></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of Tab 1 -->

                                <!-- Tab 2 -->
                                <div class="tab-accordian">

                                    <div class="titleWrapper inactive">
                                        <h3>Basic Technician Certificate in Finance and Banking</h3>

                                        <div class="collapse-icon">
                                            <div class="acc-close"> </div>
                                            <div class="acc-open"></div>
                                        </div>
                                    </div>

                                    <div id="descwrapper" class="desWrapper">
                                        <div class="titleWrapper inactive">
                                            <h3>Semester 1</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Chemistry</a></h5>

                                        </div>

                                        <div class="titleWrapper inactive">
                                            <h3>Semester 2</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Biology</a></h5>
                                        </div>






                                    </div>




                                </div>
                                <!-- end of Tab 2 -->
                                <!-- Tab 3 -->
                                <div class="tab-accordian">

                                    <div class="titleWrapper inactive">
                                        <h3>Basic Technician Certificate in Procurement and Logistics Management</h3>

                                        <div class="collapse-icon">
                                            <div class="acc-close"> </div>
                                            <div class="acc-open"></div>
                                        </div>
                                    </div>

                                    <div id="descwrapper" class="desWrapper">
                                        <div class="titleWrapper inactive">
                                            <h3>Semester 1</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Chemistry</a></h5>

                                        </div>

                                        <div class="titleWrapper inactive">
                                            <h3>Semester 2</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Biology</a></h5>
                                        </div>






                                    </div>




                                </div>
                                <!-- End of Tab 3 -->

                                <!-- Tab 4 -->

                                <div class="tab-accordian">

                                    <div class="titleWrapper inactive">
                                        <h3>Basic Technician Certificate in Business Management</h3>

                                        <div class="collapse-icon">
                                            <div class="acc-close"> </div>
                                            <div class="acc-open"></div>
                                        </div>
                                    </div>

                                    <div id="descwrapper" class="desWrapper">
                                        <div class="titleWrapper inactive">
                                            <h3>Semester 1</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Chemistry</a></h5>

                                        </div>

                                        <div class="titleWrapper inactive">
                                            <h3>Semester 2</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Biology</a></h5>
                                        </div>






                                    </div>




                                </div>
                                <!-- End of Tab 4 -->

                                <!-- Tab 5 -->
                                <div class="tab-accordian">

                                    <div class="titleWrapper inactive">
                                        <h3>Basic Technician Certificate in Computing and Information Technology</h3>

                                        <div class="collapse-icon">
                                            <div class="acc-close"> </div>
                                            <div class="acc-open"></div>
                                        </div>
                                    </div>

                                    <div id="descwrapper" class="desWrapper">
                                        <div class="titleWrapper inactive">
                                            <h3>Semester 1</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Chemistry</a></h5>

                                        </div>

                                        <div class="titleWrapper inactive">
                                            <h3>Semester 2</h3>

                                            <div class="collapse-icon">
                                                <div class="acc-close"> </div>
                                                <div class="acc-open"></div>
                                            </div>
                                        </div>
                                        <div id="descwrapper" class="desWrapper">
                                            <h5 style="padding-left:40px;text-decoration-style:none;" class="alert alert-dark" role="alert"><a href="#" style="text-decoration:none;color:grey;">Biology</a></h5>
                                        </div>






                                    </div>




                                </div>

                                <!-- End of Tab 5 -->




                                <!-- <div class="tab-accordian">

                                    <div class="titleWrapper inactive">
                                        <h3>Do I need a fraud prevention system?</h3>

                                        <div class="collapse-icon">
                                            <div class="acc-close"></div>
                                            <div class="acc-open"></div>
                                        </div>
                                    </div>

                                    <div id="descwrapper" class="desWrapper">
                                        <p>Fraud prevention can be the bane of some businesses, but also invisible to others. High chargeback rates are a clear indicator of fraudulent attacks, but some can go undetected until you implement a system.</p>
                                        <ul>
                                            <li class=""><a native="true" href="#">If you are in a high risk industry</a>, it is likely you are suffering attacks without noticing them.</li>
                                            <li class="">If user authentication is a part of your business, it is highly recommended to implement a fraud prevention to stop account takeovers and identity theft.</li>
                                            <li class="">Regulatory bodies are increasingly stringent about fraud prevention measures. To avoid hefty fines, it’s important to show you have put the best security in place.</li>
                                        </ul>
                                        <p>Finally, many organisations think they are too small to be targets of fraud. As we’ve discovered, companies of all sizes can be attacked,<a native="true" href="/use-cases/">and in an increasing number of verticals.</a></p>

                                    </div>

                                </div> -->

                                <!-- Tab 4 -->

                                <!-- <div class="tab-accordian">

                                    <div class="titleWrapper inactive">
                                        <h3>Do I need a fraud prevention system?</h3>

                                        <div class="collapse-icon">
                                            <div class="acc-close"></div>
                                            <div class="acc-open"></div>
                                        </div>
                                    </div>

                                    <div id="descwrapper" class="desWrapper">
                                        <p>Fraud prevention can be the bane of some businesses, but also invisible to others. High chargeback rates are a clear indicator of fraudulent attacks, but some can go undetected until you implement a system.</p>
                                        <ul>
                                            <li class=""><a native="true" href="#">If you are in a high risk industry</a>, it is likely you are suffering attacks without noticing them.</li>
                                            <li class="">If user authentication is a part of your business, it is highly recommended to implement a fraud prevention to stop account takeovers and identity theft.</li>
                                            <li class="">Regulatory bodies are increasingly stringent about fraud prevention measures. To avoid hefty fines, it’s important to show you have put the best security in place.</li>
                                        </ul>
                                        <p>Finally, many organisations think they are too small to be targets of fraud. As we’ve discovered, companies of all sizes can be attacked,<a native="true" href="/use-cases/">and in an increasing number of verticals.</a></p>

                                    </div>

                                </div> -->

                            </div>


                        </div>

                    </div>



                    <script>
                        jQuery(document).ready(function() {
                            jQuery('.titleWrapper').click(function() {
                                var toggle = jQuery(this).next('div#descwrapper');
                                jQuery(toggle).slideToggle("slow");
                            });
                            jQuery('.inactive').click(function() {
                                jQuery(this).toggleClass('inactive active');
                            });
                        });
                    </script>






















                    <!-- Bachelor end heading -->

                    <!-- Scroll to Top Button-->
                    <a class="scroll-to-top rounded" href="#page-top">
                        <i class="fas fa-angle-up"></i>
                    </a>

                    <!-- Logout Modal-->
                    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                    <a class="btn btn-primary" href="../../logout.php">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Bootstrap core JavaScript-->
                    <script src="vendor/jquery/jquery.min.js"></script>
                    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

                    <!-- Core plugin JavaScript-->
                    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

                    <!-- Custom scripts for all pages-->
                    <script src="js/sb-admin-2.min.js"></script>

                    <!-- Page level plugins -->
                    <script src="vendor/chart.js/Chart.min.js"></script>

                    <!-- Page level custom scripts -->
                    <script src="js/demo/chart-area-demo.js"></script>
                    <script src="js/demo/chart-pie-demo.js"></script>

    </body>

    </html>
<?php } else {
    header("Location: index.php");
} ?>