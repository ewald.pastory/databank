<?php

include "../../db_conn.php";


if (isset($_SESSION['username']) && isset($_SESSION['id'])) {   ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>DATA BANK</title>

        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <link href="css/accordian.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <style>
            html {
                -webkit-text-size-adjust: none;
                /* Prevent font scaling in landscape */
                width: 100%;
                height: 100%;
                -webkit-font-smoothing: antialiased;
                -moz-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }

            input[type="submit"] {
                -webkit-appearance: none;
            }

            *,
            *:after,
            *:before {
                box-sizing: border-box;
                margin: 0;
                padding: 0;
            }

            body {
                margin: 0;
                padding: 0;
                -webkit-font-smoothing: antialiased;
                font-family: 'Muli', sans-serif;
                font-weight: 400;
                width: 100%;
                min-height: 100%;
                color: #333333;
                width: 100%;
                height: 100%;
                background: #ecf0f4;
            }

            a {
                outline: none;
                text-decoration: none;
                color: #555;
            }

            a:hover,
            a:focus {
                outline: none;
                text-decoration: none;
            }

            img {
                border: 0;
            }

            input,
            textarea,
            select {
                outline: none;
                resize: none;
                font-family: 'Muli', sans-serif;
            }

            a,
            input,
            button {
                outline: none !important;
            }

            button::-moz-focus-inner {
                border: 0;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin: 0;
                padding: 0;
                font-weight: 700;
                color: #202342;
                font-family: 'Muli', sans-serif;
            }

            img {
                border: 0;
                vertical-align: top;
                max-width: 100%;
                height: auto;
            }

            ul,
            ol {
                margin: 0;
                padding: 0;
                list-style: none;
            }

            p {
                margin: 0 0 15px 0;
                padding: 0;
            }

            .container-fluid {
                max-width: 1900px;
            }

            /* Common Class */
            .pd-5 {
                padding: 5px;
            }

            .pd-10 {
                padding: 10px;
            }

            .pd-20 {
                padding: 20px;
            }

            .pd-30 {
                padding: 30px;
            }

            .pb-10 {
                padding-bottom: 10px;
            }

            .pb-20 {
                padding-bottom: 20px;
            }

            .pb-30 {
                padding-bottom: 30px;
            }

            .pt-10 {
                padding-top: 10px;
            }

            .pt-20 {
                padding-top: 20px;
            }

            .pt-30 {
                padding-top: 30px;
            }

            .pr-10 {
                padding-right: 10px;
            }

            .pr-20 {
                padding-right: 20px;
            }

            .pr-30 {
                padding-right: 30px;
            }

            .pl-10 {
                padding-left: 10px;
            }

            .pl-20 {
                padding-left: 20px;
            }

            .pl-30 {
                padding-left: 30px;
            }

            .px-30 {
                padding-left: 30px;
                padding-right: 30px;
            }

            .px-20 {
                padding-left: 20px;
                padding-right: 20px;
            }

            .py-30 {
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .py-20 {
                padding-top: 20px;
                padding-bottom: 20px;
            }

            .mb-30 {
                margin-bottom: 30px;
            }

            .mb-50 {
                margin-bottom: 50px;
            }

            .font-30 {
                font-size: 30px;
                line-height: 1.46em;
            }

            .font-24 {
                font-size: 24px;
                line-height: 1.5em;
            }

            .font-20 {
                font-size: 20px;
                line-height: 1.5em;
            }

            .font-18 {
                font-size: 18px;
                line-height: 1.6em;
            }

            .font-16 {
                font-size: 16px;
                line-height: 1.75em;
            }

            .font-14 {
                font-size: 14px;
                line-height: 1.85em;
            }

            .font-12 {
                font-size: 12px;
                line-height: 2em;
            }

            .weight-300 {
                font-weight: 300;
            }

            .weight-400 {
                font-weight: 400;
            }

            .weight-500 {
                font-weight: 500;
            }

            .weight-600 {
                font-weight: 600;
            }

            .weight-700 {
                font-weight: 700;
            }

            .weight-800 {
                font-weight: 800;
            }

            .text-blue {
                color: #1b00ff;
            }

            .text-dark {
                color: #000000;
            }

            .text-white {
                color: #ffffff;
            }

            .height-100-p {
                height: 100%;
            }

            .bg-white {
                background: #ffffff;
            }

            .border-radius-10 {
                -webkit-border-radius: 10px;
                -moz-border-radius: 10px;
                border-radius: 10px;
            }

            .border-radius-100 {
                -webkit-border-radius: 100%;
                -moz-border-radius: 100%;
                border-radius: 100%;
            }

            .box-shadow {
                -webkit-box-shadow: 0px 0px 28px rgba(0, 0, 0, .08);
                -moz-box-shadow: 0px 0px 28px rgba(0, 0, 0, .08);
                box-shadow: 0px 0px 28px rgba(0, 0, 0, .08);
            }

            .gradient-style1 {
                background-image: linear-gradient(135deg, #43CBFF 10%, #9708CC 100%);
            }

            .gradient-style2 {
                background-image: linear-gradient(135deg, #72EDF2 10%, #5151E5 100%);
            }

            .gradient-style3 {
                background-image: radial-gradient(circle 732px at 96.2% 89.9%, rgba(70, 66, 159, 1) 0%, rgba(187, 43, 107, 1) 92%);
            }

            .gradient-style4 {
                background-image: linear-gradient(135deg, #FF9D6C 10%, #BB4E75 100%);
            }

            /* widget style 1 */

            .widget-style1 {
                padding: 20px 10px;
            }

            .widget-style1 .circle-icon {
                width: 60px;
            }

            .widget-style1 .circle-icon .icon {
                width: 60px;
                height: 60px;
                background: #ecf0f4;
                display: flex;
                align-items: center;
                justify-content: center;
            }

            .widget-style1 .widget-data {
                width: calc(100% - 150px);
                padding: 0 15px;
            }

            .widget-style1 .progress-data {
                width: 90px;
            }

            .widget-style1 .progress-data .apexcharts-canvas {
                margin: 0 auto;
            }

            .widget-style2 .widget-data {
                padding: 20px;
            }

            .widget-style3 {
                padding: 30px 20px;
            }

            .widget-style3 .widget-data {
                width: calc(100% - 60px);
            }

            .widget-style3 .widget-icon {
                width: 60px;
                font-size: 45px;
                line-height: 1;
            }

            .apexcharts-legend-marker {
                margin-right: 6px !important;
            }
        </style>

    </head>

    <body>


        <div class="container-fluid pt-5 mt-5 pb-5">

            <!-- Widget Style 1 Start -->

            <div class="row">
                <div class="col-xl-3 mb-50">
                    <div class="gradient-style1 text-white box-shadow border-radius-10 height-100-p widget-style3">
                        <div class="d-flex flex-wrap align-items-center">
                            <div class="widget-data">
                                <div class="weight-400 font-20">Questions</div>
                                <div class="weight-300 font-30">
                                    <?php
                                    $sql = "SELECT count(*) FROM questions";
                                    $result = $conn->query($sql);

                                    while ($row = mysqli_fetch_array($result)) {
                                        echo " " . $row['count(*)'];
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="widget-icon">
                                <div class="icon"><i class="fa fa-question" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 mb-50">
                    <div class="gradient-style2 text-white box-shadow border-radius-10 height-100-p widget-style3">
                        <div class="d-flex flex-wrap align-items-center">
                            <div class="widget-data">
                                <div class="weight-400 font-20">Modules</div>
                                <div class="weight-300 font-30" id="count">
                                    <?php
                                    $sql = "SELECT count(module_id) FROM module";
                                    $result = $conn->query($sql);

                                    while ($row = mysqli_fetch_array($result)) {
                                        echo " " . $row['count(module_id)'];
                                    }
                                    ?>

                                </div>
                            </div>
                            <div class="widget-icon">
                                <div class="icon"><i class="fa fa-th-list" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 mb-50">
                    <div class="gradient-style3 text-white box-shadow border-radius-10 height-100-p widget-style3">
                        <div class="d-flex flex-wrap align-items-center">
                            <div class="widget-data">
                                <div class="weight-400 font-20">Programs</div>
                                <div class="weight-300 font-30">

                                    <?php
                                    $sql = "SELECT count(*) FROM programs";
                                    $result = $conn->query($sql);

                                    while ($row = mysqli_fetch_array($result)) {
                                        echo " " . $row['count(*)'];
                                    }
                                    ?>


                                </div>
                            </div>
                            <div class="widget-icon">
                                <div class="icon"><i class="fa fa-folder-open" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 mb-50">
                    <div class="gradient-style4 text-white box-shadow border-radius-10 height-100-p widget-style3">
                        <div class="d-flex flex-wrap align-items-center">
                            <div class="widget-data">
                                <div class="weight-400 font-20">Exams Generated</div>
                                <div class="weight-300 font-30">
                                    <?php
                                    $sql = "SELECT count(exam_id) FROM exams";
                                    $result = $conn->query($sql);

                                    while ($row = mysqli_fetch_array($result)) {
                                        echo " " . $row['count(exam_id)'];
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="widget-icon">
                                <div class="icon"><i class="fa fa-list-alt" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>




        <script>
            $('.weight-300').each(function() {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 1000,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        </script>




    </body>

    </html>
<?php } else {
    header("Location: index.php");
} ?>