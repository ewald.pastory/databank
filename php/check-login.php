<?php  
session_start();
include "../db_conn.php";

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['role'])) {

	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}

	$username = test_input($_POST['username']);
	$password = test_input($_POST['password']);
	$role = test_input($_POST['role']);
	$success=test_input($_POST['role'],['username'],['password']);

	if (empty($username)) {
		header("Location: ../index.php?error=User Name is Required");
	}else if (empty($password)) {
		header("Location: ../index.php?error=Password is Required");
	}
	else if (empty($role)){
		header("Location:../index.php?error=Role is Required");
	} else if (empty($role)) {
		header("Location:../index.php?error=Wrong role!");
	}
	else {

		// Hashing the password
		$password = md5($password);
        
        $sql = "SELECT * FROM users WHERE username='$username' AND password='$password'";
        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) === 1) {
        	// the user name must be unique
        	$row = mysqli_fetch_assoc($result);
        	if ($row['password'] === $password && $row['role'] == $role) {
        		$_SESSION['name'] = $row['name'];
        		$_SESSION['id'] = $row['id'];
        		$_SESSION['role'] = $row['role'];
        		$_SESSION['username'] = $row['username'];
				$_SESSION['email'] = $row['email'];

        		header("Location: ../template/admin/dashboard.php");

        	}else{
        		header("Location: ../index.php?error=Incorect Role!");
        	}
        }else {
        	header("Location: ../index.php?error=Incorect Username or Password!");
        }

	}
	
}else {
	header("Location: ../index.php");
}