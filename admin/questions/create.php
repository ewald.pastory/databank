<?php
require_once "../../connection.php";
if (isset($_POST['save'])) {
    $module_name = $_POST['module_name'];
    $module_code = $_POST['module_code'];
    $questions = $_POST['questions'];
$data="SELECT qns.module_id, qns.module_name, qns.module_code, qns.questions, module.id, module.module_name, module.module_code, module.questions FROM qns INNER JOIN  module ON qns.module_id = module.id";
$sql="INSERT INTO qns (module_id,module_name,module_code,questions)SELECT id, module_name, module_code, questions FROM module ";
    if (mysqli_query($conn, $sql)) {
        header("location: index.php");
        exit();
    } else {
        echo "Error: " . $sql . "
" . mysqli_error($conn);
    }
    mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Create Modules</title>
    <?php include "head.php"; ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Create Modules</h2>
                </div>
                <p>Please fill this form and submit to add employee record to the database.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group">
                        <label>Module Name</label>
                        <input type="text" name="module_name" class="form-control" value="" maxlength="50" required="">
                    </div>
                    <div class="form-group ">
                        <label>Module Code</label>
                        <input type="text" name="module_code" class="form-control" value="" maxlength="30" required="">
                    </div>
                    <div class="form-group ">
                        <label>Question</label>
                        <input type="textarea" name="questions" class="form-control" value=""  required="">
                    </div>

                    <input type="submit" class="btn btn-primary" name="save" value="submit">
                    <a href="index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</body>

</html>