<?php
// Include database connection file
require_once "../../connection.php";
if (count($_POST) > 0) {
    mysqli_query($conn, "UPDATE module set  module_name='" . $_POST['module_name'] . "', module_code='" . $_POST['module_code'] . "'  WHERE id='" . $_POST['id'] . "'");
    header("location: index.php");
    exit();
}
$result = mysqli_query($conn, "SELECT * FROM module WHERE id='" . $_GET['id'] . "'");
$row = mysqli_fetch_array($result);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Update Modules</title>
    <?php include "head.php"; ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Update Modules</h2>
                </div>
                <p>Please edit the input values and submit to update the record.</p>
                <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                    <div class="form-group">
                        <label>Module Name</label>
                        <input type="text" name="module_name" class="form-control" value="<?php echo $row["module_name"]; ?>" maxlength="50" required="">
                    </div>
                    <div class="form-group ">
                        <label>Module Code</label>
                        <input type="type" name="module_code" class="form-control" value="<?php echo $row["module_code"]; ?>" maxlength="30" required="">
                    </div>
                
                    <input type="hidden" name="id" value="<?php echo $row["id"]; ?>" />
                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</body>

</html>