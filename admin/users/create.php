<?php
require_once "../../connection.php";
if (isset($_POST['save'])) {
    $name = $_POST['name'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $password = md5($password);
    $role = $_POST['role'];
    $sql = "INSERT INTO users (name,username,email,password,role)
VALUES ('$name','$username','$email','$password','$role')";
    if (mysqli_query($conn, $sql)) {
        header("location: index.php");
        exit();
    } else {
        echo "Error: " . $sql . "
" . mysqli_error($conn);
    }
    mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <?php include "head.php"; ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Create Record</h2>
                </div>
                <p>Please fill this form and submit to add employee record to the database.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="" maxlength="50" required="">
                    </div>
                    <div class="form-group ">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" value="" maxlength="30" required="">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="" maxlength="30" required="">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" value="" maxlength="12" required="">
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-select mb-3" name="role" aria-label="Default select example">
                            <option selected value="lecturer">Lecturer</option>
                            <option selected value="examiner">Examination Officer</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary" name="save" value="submit">
                    <a href="index.php" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</body>

</html>